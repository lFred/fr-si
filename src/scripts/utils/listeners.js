import { Routes } from '@utils/routes';
import { EditProfile } from '@components/edit-profile';
import { Profile } from '@components/profile';
import { CoverImage } from '@components/cover-image';
import { UserImage } from '@components/user-image';
import { updateFields, updateName } from '@utils/update';
import { handleImages } from '@utils/tools';

/**
 * Handles all page listeners
 */
export const listeners = () => {
  const routes = new Routes();
  const links = document.querySelectorAll('[data-link]');
  const coverImage = new CoverImage();
  const userImage = new UserImage();

  routes.onRedirect(() => {
    const pageName = document.querySelector('.content__center').dataset.content;
    const editProfile = new EditProfile();
  
    if(!editProfile.storage.checkStorage()) {
      editProfile.storage.initialize();
    }
    updateFields();
  
    if(pageName === 'home') {
      const profile = new Profile();
  
      if(!editProfile.storage.checkStorage()) {
        editProfile.storage.initialize();
      }
      updateFields();
  
      profile.onEdit(function(event) {
        const fields = JSON.parse(event.currentTarget.dataset.fields);
        editProfile.openModal(event.currentTarget.parentNode);
        editProfile.addItems(fields);
      })
  
      editProfile.storage.onUpdate(updateFields);
      editProfile.storage.onNameUpdate(updateName);
    }
  });
  
  Array.from(links).forEach(item => {
    item.addEventListener('click', event => {
      routes.handleRedirect(event.currentTarget.href);
    })
  });
  
  window.addEventListener('hashchange', () => {
    routes.handleRedirect(location.hash);
  });

  coverImage.onChange(({currentTarget}) => {
    handleImages(currentTarget, coverImage);
  });

  userImage.onChange(({currentTarget}) => {
    handleImages(currentTarget, userImage);
  });
};