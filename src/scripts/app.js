import { listeners } from '@utils/listeners';
import { Reviews } from '@components/reviews';
import { Routes } from '@utils/routes';

listeners();
const routes = new Routes();
const reviews = new Reviews();
const page = location.hash.substring(1);

routes.redirectPage(page);
reviews.updateStars();