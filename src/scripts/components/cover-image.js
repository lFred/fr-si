export class CoverImage {

  /**
   * Sets Cover Image input and image elements 
   */
  constructor() {
    this._input = document.querySelector('.cover-image__input');
    this._image = document.querySelector('.cover-image__image');
  }

  /**
   * Adds a listener to the Cover Image input
   * @param {function} callback Callback function
   */
  onChange(callback) {
    this._input.addEventListener('change', callback);
  }

  /**
   * Gets the image HTML element of the Cover Image
   */
  get image() {
    return this._image;
  }

  /**
   * Sets the file for the Cover image img HTML element
   * @param {file | Blob} fields file or Blob param
   */
  set file(file) {
    this._image.file = file;
  }

  /**
   * Sets the src for the Cover image img HTML element
   * @param {string} src Image src
   */
  set src(src) {
    this._image.src = src;
  }
}