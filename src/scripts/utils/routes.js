import { cleanNodes } from './tools';

export class Routes {

  /**
   * Sets the location url, content element, all links element and creates an event for the page redirect.
   */
  constructor() {
    this.url = window.location;
    this.redirect = new Event('page-redirect');
    this.content = document.querySelector('.content');
    this.links = document.querySelectorAll('[data-link]');
  }

  /**
   * Appends a template to the content according to the page parameter and dispatch the page-redirect event
   * @param {string} page name of the page
   */
  redirectPage(page) {
    if(document.getElementById(`${page}-content`) === null) {
      page = 'home';
    }

    const pageTemplate = document.getElementById(`${page}-content`);
    const pageContent = document.importNode(pageTemplate.content, true);
    cleanNodes(this.content);

    this.content.appendChild(pageContent);
    this.updateMenuLink(page);
    this.dispatchRedirect();
  }

  /**
   * Handles the page string before sending it to redirectPage
   * @param {string} url page string
   */
  handleRedirect(url) {
    const sharpIndex = url.indexOf("#"); 
    const page = (sharpIndex >= 0) ? url.substring(sharpIndex + 1) : "";
    this.redirectPage(page);
  }

  /**
   * Adds a class to the menu icon according to the page
   * @param {string} url page string
   */
  updateMenuLink(page) {
    Array.from(this.links).map(item => {
      item.classList.remove('menu__link--on');

      if(item.dataset.link.toLowerCase() === page) {
        item.classList.add('menu__link--on');
      }
    });
  }
  
  /**
   * Dispatch the redirect event
   * @param {string} url page string
   */
  dispatchRedirect() {
    window.dispatchEvent(this.redirect);
  }

  /**
   * Listen to the redirect event
   * @param {function} callback Callback function
   */
  onRedirect(callback) {
    window.addEventListener('page-redirect', callback);
  }
}