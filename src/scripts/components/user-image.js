export class UserImage {

  /**
   * Sets User Image input and image elements
   * @param {JSON} fields JSON object with all fields {label, name, type, parent: optional}
   */
  constructor() {
    this._input = document.querySelector('.user__image-input');
    this._image = document.querySelector('.user__image');
  }

  /**
   * Listen to change events on the input element
   * @param {function} callback Callback function
   */
  onChange(callback) {
    this._input.addEventListener('change', callback);
  }

  /**
   * Gets the image HTML element
   */
  get image() {
    return this._image;
  }

  /**
   * Sets the file for the User Image img HTML element
   * @param {file | Blob} file file or Blob param
   */
  set file(file) {
    this._image.file = file;
  }

  /**
   * Sets the src for the User Image Image img HTML element
   * @param {string} src Image src
   */
  set src(src) {
    this._image.src = src;
  }
}