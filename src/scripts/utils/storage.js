export class ProfileStorage {
  constructor() {
    this.update = new Event('profile-update');
    this.nameUpdate = new CustomEvent('profile-name-update');
  }

  /**
   * Initialize localStorage with dummy data
   */
  initialize() {
    localStorage.setItem('profile-fullname', "Jessica Parker");
    localStorage.setItem('profile-first-name', "Jessica");
    localStorage.setItem('profile-last-name', "Parker");
    localStorage.setItem('profile-website', "www.seller.com");
    localStorage.setItem('profile-phone', "(949) 325 - 68594");
    localStorage.setItem('profile-address', "Newport Beach, CA");
    this.dispatchNameUpdate();
    this.dispatchUpdate();
  }

  /**
   * Set an item to the localStorage and fire the nameUpdate event when needed
   * @param {string, string, boolean}
   */
  setAttribute(key, value, fireNameEvent = true) {
    localStorage.setItem(`profile-${key}`, value);

    if(key === "fullname" || key === "first-name" || key === "last-name") {
      this.nameUpdate = new CustomEvent('profile-name-update', {'detail': {
        [key]: value
      }
      });

      if(fireNameEvent) {
        this.dispatchNameUpdate();
      }
    }

    this.dispatchUpdate();
  }

  /**
   * Get a localStorage item according to the key
   * @param {string} key Key string
   */
  getAttribute(key) {
    return localStorage.getItem(`profile-${key}`);
  }

  /**
   * Dispatch the update event
   */
  dispatchUpdate() {
    window.dispatchEvent(this.update);
  }

  /**
   * Dispatch the name update event
   */
  dispatchNameUpdate() {
    window.dispatchEvent(this.nameUpdate);
  }

  /**
   * Listen to the update event
   * @param {function} callback Callback function
   */
  onUpdate(callback) {
    window.addEventListener('profile-update', callback);
  }

  /**
   * Listen to the name update event
   * @param {function} callback Callback function
   */
  onNameUpdate(callback) {
    window.addEventListener('profile-name-update', callback);
  }

  /**
   * Checks if the fullname item exists in local storage
   */
  checkStorage() {
    return (localStorage.getItem('profile-fullname') === null) ? false : true;
  }
}