const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const loaders = require('./loaders.dev');
const plugins = require('./plugins');

const config = merge(common, {
  mode: "development",
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: '[name].[contentHash].js',
  },
  module: {
    rules : [
      loaders.SASSLoader,
    ]
  },
  plugins: [
    plugins.HtmlWebpackDev,
  ]
});

module.exports = config;