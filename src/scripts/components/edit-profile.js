import { ProfileStorage } from '@utils/storage';
import { cleanNodes } from '@utils/tools';

export class EditProfile {

  /**
   * Sets EditProfile modal template, fieldsList, and a storage instance
   */
  constructor() {
    this.modalTemplate = document.getElementById('edit-profile');
    this.fieldsList = [];
    this.storage = new ProfileStorage();
  }

  /**
   * Initializes all elements and events
   */
  initialize() {
    this.fieldsList = this.modal.querySelector('.edit-profile__list');
    this.saveButton = this.modal.querySelector('.edit-profile__button--save');
    this.cancelButton = this.modal.querySelector('.edit-profile__button--cancel');
    this.itemTemplate = document.getElementById('edit-profile__item');

    this.saveButton.addEventListener('click', () => {
      this.saveModal();
    });
    
    this.cancelButton.addEventListener('click', () => {
      this.closeModal();
    });
  }

  /**
   * Remove all old items and adds new ones to the Edit Profile modal
   * @param {JSON} fields JSON object with all fields {label, name, type, parent: optional}
   */
  addItems(fields) {
    cleanNodes(this.fieldsList);

    fields.forEach(({label, name, type, parent}) => {
      const item = document.importNode(this.itemTemplate.content, true);
      const inputLabel = item.querySelector('.edit-profile__label');
      const field = item.querySelector('.edit-profile__input');
      const value = this.storage.getAttribute(name);

      inputLabel.textContent = label;
      inputLabel.htmlFor = name;
      field.name = name;
      field.id = name;
      field.type = type;
      field.value = value;
      field.dataset.parent = parent;

      if(value !== "") {
        field.classList.add('edit-profile__input--on');
      }
      this.fieldsList.appendChild(item);

      field.addEventListener("focus", ({currentTarget}) => this.handleInputEmpty(currentTarget));
      field.addEventListener("blur", ({currentTarget}) => this.handleInputEmpty(currentTarget));
      field.addEventListener("keyup", ({currentTarget}) => {
        if(event.key === "Escape") {
          this.closeModal();
        } else 
        if(event.key === "Enter"){
          this.saveModal();
        } else {
          this.handleInputEmpty(currentTarget);
        } 
      });
    });
  }

  /**
   * Opens the Edit Profile modal
   * @param {JSON} parent JSON object with all fields {label, name, type, parent: optional}
   */
  openModal(parent) {
    this.removeOldModal();

    const modal = document.importNode(this.modalTemplate.content, true);
    parent.appendChild(modal);
    this.modal = document.querySelector('.edit-profile');
    this.initialize();
  }

  /**
   * Closes the Edit Profile modal
   */
  closeModal() {
    this.removeOldModal();
  }

  /**
   * Saves the Edit Profile modal and trigger a callback function
   */
  saveModal() {
    this.modal.classList.remove('edit-profile--on');
    const fields = this.fieldsList.querySelectorAll('.edit-profile__input');

    Array.from(fields).forEach(({name, value}) => {
      this.storage.setAttribute(name, value);
    });

    this.closeModal();
  }

  /**
   * Handles input empty state, adding classes to it
   * @param {HTMLElement} input Input HTML Element
   */
  handleInputEmpty(input) {
    if(input.value === "") {
      input.classList.remove('edit-profile__input--on');
    } else {
      input.classList.add('edit-profile__input--on');
    }
  }

  /**
   * Removes the old modal from the page
   */
  removeOldModal() {
    const old = document.querySelector('.edit-profile');
    if(old !== null) {
      old.remove();
    }
  }
}