const path = require('path');

const SASSLoader = {
  test: /\.scss$/,
  exclude: /node_modules/,
  use: [
    {
      loader: 'style-loader',
    },
    {
      loader: 'css-loader',
      options: {importLoaders: 1},
    },
    {
      loader: 'postcss-loader',
      options: {
        config: {
          path: path.resolve(__dirname, 'postcss.config.js')
        }
      },
    },
    {
      loader: 'sass-loader',
    }
  ],
};

module.exports = {
  SASSLoader,
};