export class Reviews {

  /**
   * Sets reviews, scoreeElement, and stars value
   */
  constructor() {
    this.reviews = document.querySelector('.reviews');
    this.scoreElement = this.reviews.querySelector('.reviews__stars');
    this.stars = this.scoreElement.dataset.score;
  }

  /**
   * Appends a star template to the stars element according to its score 
   */
  updateStars() {
    const template = document.querySelector(`.template--stars[data-score="${this.stars}"]`);
    const item = document.importNode(template.content, true);
    this.scoreElement.append(item);
  }
}