const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const plugins = require('./plugins');
const loaders = require('./loaders.common');

const config = {
  entry: {
    main: [path.resolve(__dirname, '../src/scripts/app.js'), path.resolve(__dirname, '../src/styles/app.scss')],
    vendor: [path.resolve(__dirname, '../src/scripts/vendor.js')]
  },
  
  plugins: [
    plugins.StyleLintPlugin,
    plugins.MiniCssExtractPlugin,
    new CleanWebpackPlugin(),
  ],

  module: {
    rules : [
      loaders.JSLoader,
      loaders.ESLintLoader,
      loaders.HTMLLoader,
      loaders.FilesLoader,
    ]
  },
};

module.exports = config;