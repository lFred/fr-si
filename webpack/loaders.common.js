const path = require('path');

const JSLoader = {
  test: /\.js$/,
  exclude: /node_modules/,
  use: {
    loader: 'babel-loader',
    options: {
      "plugins": [
        [
          "module-resolver",
          {
            "root": ["../"],
            "alias": {
              "@components": "./src/scripts/components",
              "@utils": "./src/scripts/utils",
              "@images": "./src/images",
              "@styles": "./src/styles"
            }
          }
        ]
      ]
    }    
  }
};

const ESLintLoader = {
  test: /\.js$/,
  enforce: 'pre',
  exclude: /node_modules/,
  use: {
    loader: 'eslint-loader',
    options: {
      configFile: path.resolve(__dirname, '.eslintrc')
    },
  }
};

const HTMLLoader = {
  test: /\.html$/,
  use: [
    {
      loader: "html-loader",
      options: {
        interpolate: true,
      },
    },
  ],
}

const FilesLoader = {
  test: /\.(gif|png|jpe?g|svg)$/i,
  use: [
    {
      loader: "file-loader",
      options: {
        name: "[name].[hash].[ext]",
        outputPath: "assets",
        esModule: false
      }
    },
    {
      loader: "image-webpack-loader",
      options: {
        mozjpeg: {
          progressive: true,
          quality: 65
        },
        optipng: {
          enabled: false,
        },
        pngquant: {
          quality: [0.65, 0.90],
          speed: 4
        },
        gifsicle: {
          interlaced: false,
        }
      }
    }
  ]}

module.exports = {
  JSLoader,
  ESLintLoader,
  HTMLLoader,
  FilesLoader
};