export class Profile {

  /**
   * Sets profile edit button
   */
  constructor() {
    this.profileButtons = document.querySelectorAll('.profile__edit');
  }

  /**
   * Creates a listener for all edit buttons
   * @param {function} callback Callback function
   */
  onEdit(callback) {
    Array.from(this.profileButtons).forEach(item => {
      item.addEventListener('click', (event) => {
        callback(event);
      }, true);
    })
  }
}