const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const SASSLoader = {
  test: /\.scss$/,
  exclude: /node_modules/,
  use: [
    {
      loader: MiniCssExtractPlugin.loader,
    },
    {
      loader: 'css-loader',
      options: {importLoaders: 1},
    },
    {
      loader: 'postcss-loader',
      options: {
        config: {
          path: path.resolve(__dirname, 'postcss.config.js'),
        }
      },
    },
    {
      loader: 'sass-loader',
    }
  ],
};

module.exports = {
  SASSLoader
};