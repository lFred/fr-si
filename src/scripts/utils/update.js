import { EditProfile } from '@components/edit-profile';
import { ProfileStorage } from './storage';

const editProfile = new EditProfile();
const storage = new ProfileStorage();

/**
* Updates all elements that has data-storage param with dynamic data
*/
export const updateFields = () => {
  const textElements = document.querySelectorAll('[data-storage^="profile-"]');
  const imageElements = document.querySelectorAll('[data-storage-image^="profile-"]');

  Array.from(textElements).forEach((item) => {
    const initialIndex = item.dataset.storage.indexOf('profile-');
    const key = item.dataset.storage.substring(initialIndex + 8);

    item.textContent = editProfile.storage.getAttribute(key);
  });

  Array.from(imageElements).forEach((item) => {
    const storageKey = item.dataset.storageImage;
    const initialIndex = storageKey.indexOf('profile-');
    const key = item.dataset.storageImage.substring(initialIndex + 8);

    if(editProfile.storage.getAttribute(key)) {
      item.src = editProfile.storage.getAttribute(key);
      item.alt = storageKey;
    }
  });
}

/**
* Upload the image to the local storage
*/
export const setImages = (newImage, imageObj, storageKey) => {
  const reader = new FileReader();
  imageObj.file = newImage;
  
  reader.onload = (imageObj => e => {
    const src = e.target.result;
    imageObj.src = src;
    storage.setAttribute(storageKey, src);
  })(imageObj);
    
  reader.readAsDataURL(newImage);
};

/**
* Handles fullname, first-name, and last-name local storage items
* @param {JSON} detail Event's detail
*/
export const updateName = ({detail}) => {
  if(detail.fullname) {
    const spaceIndex = (detail.fullname.indexOf(" ") === -1) ? detail.fullname.length : detail.fullname.indexOf(" ");
    const firstName = detail.fullname.substr(0, spaceIndex);
    const lastName = detail.fullname.substr(spaceIndex + 1);
    storage.setAttribute('first-name', firstName, false);
    storage.setAttribute('last-name', lastName, false);
  } else

  if(detail["first-name"]) {
    const lastName = storage.getAttribute("last-name");
    storage.setAttribute('fullname', `${detail["first-name"]} ${lastName}`, false);
  } else 
  
  if(detail["last-name"]) {
    const firstName = storage.getAttribute("first-name");
    storage.setAttribute('fullname', `${firstName} ${detail["last-name"]}`, false);
  }

}