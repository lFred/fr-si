const path = require('path');
const _MiniCssExtractPlugin = require('mini-css-extract-plugin');
const _StyleLintPlugin = require('stylelint-webpack-plugin');
const _HtmlWebpackPlugin = require('html-webpack-plugin');

const MiniCssExtractPlugin = new _MiniCssExtractPlugin({
  filename: '[name].[contentHash].css',
  chunkFilename: '[id].css'
});

const StyleLintPlugin = new _StyleLintPlugin({
  configFile: path.resolve(__dirname, 'stylelint.config.js'),
  context: path.resolve(__dirname, '../src/styles'),
  files: '**/*.scss',
  failOnError: false,
  quiet: false,
});

const HtmlWebpackDev = new _HtmlWebpackPlugin({
  template: path.resolve(__dirname, '../src/views/index.html'),
});

const HtmlWebpackProd = new _HtmlWebpackPlugin({
  template: path.resolve(__dirname, '../src/views/index.html'),
  minify: {
    removeAttributeQuotes: true,
    collapseWhitespace: true,
    removeComments: true,
  }
});

module.exports = {
  MiniCssExtractPlugin,
  StyleLintPlugin,
  HtmlWebpackDev,
  HtmlWebpackProd,
};