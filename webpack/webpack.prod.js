const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const loaders = require('./loaders.prod');
const plugins = require('./plugins');

const config = merge(common, {
  mode: "production",
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: '[name].[contentHash].js',
  },
  module: {
    rules : [
      loaders.SASSLoader,
    ]
  },
  plugins: [
    plugins.HtmlWebpackProd,
  ]
});

module.exports = config;