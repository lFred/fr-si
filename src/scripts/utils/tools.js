import { setImages } from "./update";

/**
* Removes all nodes within an element
* @param {NodeList} list Element's node list
*/
export const cleanNodes = list => {
  if(list !== null && list !== undefined) {
    while(list.firstChild) {
      list.removeChild(list.firstChild);
    }
  }
}

/**
* Handles dynamic images of the page
* @param {HTMLElement, Object}
*/
export const handleImages = (currentTarget, imageObj) => {
  const newImage = currentTarget.files[0];
  const dataStorage = imageObj.image.dataset.storageImage;
  const profileIndex = dataStorage.indexOf("profile-");
  const storageKey = dataStorage.substr(profileIndex + 8);

  setImages(newImage, imageObj, storageKey);
}